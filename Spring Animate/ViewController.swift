//
//  ViewController.swift
//  Spring Animate
//
//  Created by Алексей on 22/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet fileprivate weak var blueView: UIView!
    @IBOutlet fileprivate weak var yellowView: UIView!
    
    @IBOutlet fileprivate weak var topConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var middleContraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var bottomContraint: NSLayoutConstraint!
    
    private var lastPosition: CGPoint?
    
    private let animationDuration: Double = 2.0
    private let animationDelay: Double = 0.0
    private let animationDamping: CGFloat = 1.0
    private let animationVelocity: CGFloat = 10.0
    
    private var defaultOffset: CGFloat{
        let offset = ((view.frame.height - blueView.frame.height - yellowView.frame.height) / 3)
        return offset
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaultPostion()
        configurePans()
    }
    
    private func configurePans(){
        let blueViewPan = UIPanGestureRecognizer(target: self, action: #selector(handleYellowPan(_:)))
        blueViewPan.minimumNumberOfTouches = 1
        blueViewPan.maximumNumberOfTouches = 1
        blueView.addGestureRecognizer(blueViewPan)
        
        let yellowViewPan = UIPanGestureRecognizer(target: self, action: #selector(handleYellowPan(_:)))
        yellowViewPan.minimumNumberOfTouches = 1
        yellowViewPan.maximumNumberOfTouches = 1
        yellowView.addGestureRecognizer(yellowViewPan)
    }
    
    private func defaultPostion(){
        topConstraint.constant = defaultOffset
        middleContraint.constant = defaultOffset
        bottomContraint.constant = defaultOffset
    }
    
    @objc
    private func handleYellowPan(_ pan: UIPanGestureRecognizer){
        switch pan.state {
        case .began:
            lastPosition = pan.translation(in: view)
            
        case .changed:
            changedPosition(of: pan)
            lastPosition = pan.translation(in: view)
            
        case .ended:
            self.lastPosition = nil
            springAnimate()
            
        default:
            return
        }
    }
    
    private func changedPosition(of pan: UIPanGestureRecognizer){
        let point = pan.translation(in: view)
        guard let view = pan.view else { return }
        
        switch view {
        case yellowView:
            changeYellow(point: point)
            
        case blueView:
            changeBlue(point: point)
            
        default:
            return
        }
    }
    
    private func changeBlue(point: CGPoint){
        guard  let lastPosition = self.lastPosition else {
            return
        }
        bottomContraint.constant = bottomContraint.constant + ((lastPosition.y - point.y) / 2)
        middleContraint.constant = middleContraint.constant + ((lastPosition.y - point.y) / 2)
        topConstraint.constant = topConstraint.constant - (lastPosition.y - point.y)
    }
    
    private func changeYellow(point: CGPoint){
        guard  let lastPosition = self.lastPosition else {
            return
        }
        bottomContraint.constant = bottomContraint.constant + (lastPosition.y - point.y)
        middleContraint.constant = middleContraint.constant - ((lastPosition.y - point.y) / 2)
        topConstraint.constant = topConstraint.constant - ((lastPosition.y - point.y) / 2)
    }
    
    private func springAnimate(){
        defaultPostion()
        UIView.animate(withDuration: animationDuration,
                       delay: animationDelay,
                       usingSpringWithDamping: animationDamping,
                       initialSpringVelocity: animationVelocity,
                       options: [.curveEaseInOut],
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
        })
    }
}
